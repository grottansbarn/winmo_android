0.9.0 :: 9 :: 0 :: 9 :: 2.0
SDL2 was configured with the following options:
Platform: Android-1
64-bit:   FALSE
Compiler: /home/huskerdu/Android/Sdk/ndk-bundle/toolchains/llvm/prebuilt/linux-x86_64/bin/clang
Subsystems:
  Atomic:	ON
  Audio:	ON
  Video:	ON
  Render:	ON
  Events:	ON
  Joystick:	ON
  Haptic:	ON
  Power:	ON
  Threads:	ON
  Timers:	ON
  File:	ON
  Loadso:	ON
  CPUinfo:	ON
  Filesystem:	ON
  Dlopen:	ON
  Sensor:	ON
Options:
  3DNOW                  (Wanted: ON): OFF
  ALSA                   (Wanted: ON): OFF
  ALSA_SHARED            (Wanted: ON): OFF
  ALTIVEC                (Wanted: ON): OFF
  ARTS                   (Wanted: ON): OFF
  ARTS_SHARED            (Wanted: ON): OFF
  ASSEMBLY               (Wanted: ON): ON
  ASSERTIONS             (Wanted: auto): auto
  CLOCK_GETTIME          (Wanted: OFF): OFF
  DIRECTFB_SHARED        (Wanted: OFF): OFF
  DIRECTX                (Wanted: OFF): OFF
  DISKAUDIO              (Wanted: ON): ON
  DUMMYAUDIO             (Wanted: ON): ON
  ESD                    (Wanted: ON): OFF
  ESD_SHARED             (Wanted: ON): OFF
  FUSIONSOUND            (Wanted: OFF): OFF
  FUSIONSOUND_SHARED     (Wanted: OFF): OFF
  GCC_ATOMICS            (Wanted: ON): ON
  INPUT_TSLIB            (Wanted: ON): OFF
  JACK                   (Wanted: ON): OFF
  JACK_SHARED            (Wanted: ON): OFF
  KMSDRM_SHARED          (Wanted: ON): OFF
  LIBC                   (Wanted: ON): ON
  LIBSAMPLERATE          (Wanted: ON): OFF
  LIBSAMPLERATE_SHARED   (Wanted: ON): OFF
  MIR_SHARED             (Wanted: ON): OFF
  MMX                    (Wanted: ON): OFF
  NAS                    (Wanted: ON): OFF
  NAS_SHARED             (Wanted: ON): OFF
  OSS                    (Wanted: ON): OFF
  PTHREADS               (Wanted: ON): ON
  PTHREADS_SEM           (Wanted: ON): ON
  PULSEAUDIO             (Wanted: ON): OFF
  PULSEAUDIO_SHARED      (Wanted: ON): OFF
  RENDER_D3D             (Wanted: OFF): OFF
  RPATH                  (Wanted: ON): OFF
  SDL_DLOPEN             (Wanted: ON): ON
  SDL_STATIC_PIC         (Wanted: OFF): OFF
  SDL_TEST               (Wanted: OFF): OFF
  SNDIO                  (Wanted: ON): OFF
  SSE                    (Wanted: ON): OFF
  SSE2                   (Wanted: OFF): OFF
  SSE3                   (Wanted: OFF): OFF
  SSEMATH                (Wanted: OFF): ON
  VIDEO_COCOA            (Wanted: OFF): OFF
  VIDEO_DIRECTFB         (Wanted: OFF): OFF
  VIDEO_DUMMY            (Wanted: ON): ON
  VIDEO_KMSDRM           (Wanted: ON): OFF
  VIDEO_MIR              (Wanted: ON): OFF
  VIDEO_OPENGL           (Wanted: ON): OFF
  VIDEO_OPENGLES         (Wanted: ON): ON
  VIDEO_RPI              (Wanted: ON): OFF
  VIDEO_VIVANTE          (Wanted: ON): OFF
  VIDEO_VULKAN           (Wanted: ON): ON
  VIDEO_WAYLAND          (Wanted: ON): OFF
  VIDEO_WAYLAND_QT_TOUCH (Wanted: ON): OFF
  VIDEO_X11              (Wanted: ON): OFF
  VIDEO_X11_XCURSOR      (Wanted: ON): OFF
  VIDEO_X11_XINERAMA     (Wanted: ON): OFF
  VIDEO_X11_XINPUT       (Wanted: ON): OFF
  VIDEO_X11_XRANDR       (Wanted: ON): OFF
  VIDEO_X11_XSCRNSAVER   (Wanted: ON): OFF
  VIDEO_X11_XSHAPE       (Wanted: ON): OFF
  VIDEO_X11_XVM          (Wanted: ON): OFF
  WASAPI                 (Wanted: OFF): OFF
  WAYLAND_SHARED         (Wanted: ON): OFF
  X11_SHARED             (Wanted: ON): OFF
 CFLAGS:        -isystem /home/huskerdu/Android/Sdk/ndk-bundle/sysroot/usr/include/arm-linux-androideabi -g -DANDROID -ffunction-sections -funwind-tables -fstack-protector-strong -no-canonical-prefixes -march=armv7-a -mfloat-abi=softfp -mfpu=vfpv3-d16 -mthumb -Wa,--noexecstack -Wformat -Werror=format-security  -idirafter /home/huskerdu/Programs/SDL2-2.0.9/src/video/khronos
 EXTRA_CFLAGS:  -Wshadow -fvisibility=hidden -Wdeclaration-after-statement -Werror=declaration-after-statement -Wall 
 EXTRA_LDFLAGS: -Wl,--no-undefined
 EXTRA_LIBS:    m;/home/huskerdu/Android/Sdk/ndk-bundle/platforms/android-18/arch-arm/usr/lib/libdl.so;/home/huskerdu/Android/Sdk/ndk-bundle/platforms/android-18/arch-arm/usr/lib/liblog.so;/home/huskerdu/Android/Sdk/ndk-bundle/platforms/android-18/arch-arm/usr/lib/libandroid.so;/home/huskerdu/Android/Sdk/ndk-bundle/platforms/android-18/arch-arm/usr/lib/libGLESv1_CM.so;/home/huskerdu/Android/Sdk/ndk-bundle/platforms/android-18/arch-arm/usr/lib/libGLESv2.so
 Build Shared Library: ON
 Build Static Library: ON
 Build Static Library with Position Independent Code: OFF
If something was not detected, although the libraries
were installed, then make sure you have set the
CFLAGS and LDFLAGS environment variables correctly.
Configuring done
